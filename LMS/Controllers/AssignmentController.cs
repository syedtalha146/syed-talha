﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Controllers
{
    public class AssignmentController : Controller
    {
        // GET: Assignment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddAssignment()
        {
            return View();
        }

        public ActionResult Field()
        {
            return View();
        }

        public ActionResult Assignments()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult StudentClass()
        {
            return View();
        }

        public ActionResult StudentAssignment()
        {
            return View();
        }

        public ActionResult UploadAssignment()
        {
            return View();
        }
        
    }
}