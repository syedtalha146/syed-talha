﻿using LMS.Models.Loginuser;
using LMS.Models.Logs;
using LMS.Models.Tasks;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;

namespace LMS.Controllers
{
    public class TaskController : Controller
    {
        // GET: Task
        public ActionResult Index()
        {
            if (Session["RecID"] != null)
            {
                try
                {
                    bool b;
                    List<Permissions> pmlList = (List<Permissions>)Session["Permissionlist"];
                    b = Common.checkpermission(pmlList,"9");
                    if (b == true)
	                {
                        string UserID = System.Web.HttpContext.Current.Session["RecID"].ToString();
                        DataTable dt = Loginvalid.PermCheck(UserID);
                        List<Permissions> permission = new List<Permissions>();
                        foreach (DataRow dc in dt.Rows)
                        {
                            Permissions perm = new Permissions();
                            //byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(dc["PermID"].ToString());
                            //perm.PermID = Convert.ToBase64String(b);
                            perm.PermID = dc["PermID"].ToString();
                            perm.Permission = dc["Permission"].ToString();
                            perm.PermissionDes = dc["PermissionDes"].ToString();

                            permission.Add(perm);
                        }

                        ViewData["permissions"] = permission;
                        return View();
	                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            else
            {
                return View("SessionExpire");
            }
        }

        public ActionResult Tasks()
        {
            if (Session["RecID"] != null)
            {
                bool b;
                List<Permissions> pmList = (List<Permissions>)Session["Permissionlist"];
                b = Common.checkpermission(pmList, "10");
                if (b == true)
                {
                    return View();
                }
                else
                {
                    return View("accessdenied");
                }

            }
            else
            {
                return View("SessionExpire");
            }
        }

        public ActionResult AddTask()
        {
            if (Session["RecID"] != null)
            {
                bool b;
                List<Permissions> pmList = (List<Permissions>)Session["Permissionlist"];
                b = Common.checkpermission(pmList, "8");
                if (b == true)
                {
                    return View();
                }
                else
                {
                    return View("accessdenied");
                }

            }
            else
            {
                return View("SessionExpire");
            }
        }

        public ActionResult MyTask()
        {
            if (Session["RecID"] != null)
            {
                bool b;
                List<Permissions> pmList = (List<Permissions>)Session["Permissionlist"];
                b = Common.checkpermission(pmList, "9");
                if (b == true)
                {
                    try
                    {
                        string UserID = Session["RecID"].ToString();
                        DataTable dt = tasks.GetMyTasks(UserID);
                        List<MyTasks> mt = new List<MyTasks>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            MyTasks t = new MyTasks();
                            t.TaskID = dr["TaskID"].ToString();
                            t.Task = dr["Task"].ToString();
                            t.StatusID = dr["StatusID"].ToString();
                            t.AssignedTo = dr["AssignedTo"].ToString();
                            t.AttachmentBy = dr["AttachmentBy"].ToString();
                            t.AttachmentTo = dr["AttachmentTo"].ToString();
                            t.TimeStamp = dr["TimeStamp"].ToString();
                            mt.Add(t);
                        }
                        ViewData["mytask"] = mt;
                        return View();
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                }
                else
                {
                    return View("accessdenied");
                }

            }
            else
            {
                return View("SessionExpire");
            }
        }

        [HttpGet]
        public ActionResult AttachmentsDetails(int ProfileID)
        {
            if (Session["username"] == null)
            {
                return View("sessionexpire");
            }
            try
            {
                //Attach getimage = new Attach();
                DataTable dt = tasks.GetAttach(ProfileID);
                List<Attach> dl = new List<Attach>();
                foreach (DataRow item in dt.Rows)
                {
                    Attach a = new Attach();
                    a.Attachment = item["Attachment"].ToString();
                    dl.Add(a);
                }
                ViewData["attchmntDetails"] = dl;

            }
            catch (Exception)
            {

                return Content("Something Went Wrong");
            }
            return PartialView();
        }

        public ActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        [Obsolete]
        public ActionResult AddTask(FormCollection form, IEnumerable<HttpPostedFileBase> Attachment)
        {
            if (Session["RecID"] != null)
            {
                bool b;
                List<Permissions> pmList = (List<Permissions>)Session["Permissionlist"];
                b = Common.checkpermission(pmList, "10");
                if (b == true)
                {
                    string UserID = System.Web.HttpContext.Current.Session["RecID"].ToString();
                    string Task = form["Task"];
                    string AssignedTo = form["User"];
                    //int count1 = 0;
                    try
                    {
                        foreach (var file in Attachment)
                        {
                            if (file != null)
                            {
                                string root = Server.MapPath("~/Attachments/Task");
                                bool exists = Directory.Exists(root);
                                if (!exists)
                                {
                                    Directory.CreateDirectory(root);
                                }
                                string im = file.FileName;
                                //im = im.Replace(" ", "");
                                string filename = UserID + "-" + im;
                                string path = ConfigurationManager.AppSettings["taskpath"];
                                string filepath = Path.Combine(path, filename);
                                file.SaveAs(Path.Combine(root, filename));

                                string Result = tasks.AddTask(UserID, AssignedTo, Task, filepath);

                                if (Result == "1")
                                {
                                    StringBuilder sb_text = new StringBuilder();
                                    string Email = tasks.GetEmail(AssignedTo);
                                    sb_text.AppendLine("<div style='background-color:#ffffff'>");
                                    sb_text.AppendLine("<p style='text-align:center;background-color:#00b050;'><span style='color: #ffffff;'>Task Assigned By " + Session["Name"] + "<br /></span></p>");
                                    sb_text.AppendLine("<br />");
                                    sb_text.AppendLine("<br />");
                                    sb_text.AppendLine("<p style='text-align:center;'><strong><span style='color: #0070c0;font-size:large;'>Gear up you now you have a Task to Complete!</span></strong></p>");
                                    sb_text.AppendLine("<br />");
                                    sb_text.AppendLine("<p style='text-align:center;'><strong><span style='color: #ed7d31;font-size:large;'>Task: " + Task + "</span><span style='color: #00b050;font-size:large;'></span></strong></p>");
                                    sb_text.AppendLine("<br />");
                                    sb_text.AppendLine("<p style='text-align:center;'><strong><span style='color: #ed7d31;font-size:large;'>If you need any clearification regarding this Task Please ask to " + Session["Name"] + " on Email Address: " + Session["Username"] + ".</span></strong></p>");
                                    sb_text.AppendLine("<br />");
                                    sb_text.AppendLine("<br />");
                                    sb_text.AppendLine("<p style='text-align:center;background-color:#000000;'><span style='color:#ffffff;'>LMS. All Rights Reserved</span></p>");
                                    sb_text.AppendLine("</div>");
                                    sb_text.AppendLine("<p style='text-align:center;'><span>-----This is a System Generated Email. Please Do Not Reply-----</span></p>");

                                    Models.Gmail.SendEmail(ConfigurationManager.AppSettings["EmailFrom"].ToString(), ConfigurationManager.AppSettings["appdevPass"].ToString(), Email, "Task is been waiting for you", sb_text.ToString(), MailFormat.Html, "");
                                }

                                if (Result == "0")
                                {
                                    return Json(new { Message = "Connection Timed Out ! Try Again ", response = "0" });
                                }
                                else
                                {
                                    return Json(new { Message = "Task Assigned and Email Submitted Successfully! ", response = "1" });
                                }


                                //count1++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }



                }
                else
                {
                    return View("accessdenied");
                }

            }
            else
            {
                return View("SessionExpire");
            }
        }
    }
}