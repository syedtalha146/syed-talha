﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Controllers
{
    public class FieldController : Controller
    {
        // GET: Field
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IT()
        {
            return View();
        }

        public ActionResult Student()
        {
            return View();
        }

        public ActionResult Instructors()
        {
            return View();
        }

        public ActionResult Recovery()
        {
            return View();
        }

        public ActionResult Users()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult UserUpdate()
        {
            return View();
        }

        public ActionResult NewUser()
        {
            return View();
        }

        public ActionResult RoleAssignment()
        {
            return View();
        }

        public ActionResult PermAssignment()
        {
            return View();
        }

        public ActionResult NewOrganization()
        {
            return View();
        }

        public ActionResult ManageOrganization()
        {
            return View();
        }

        public ActionResult UpdateOrganization()
        {
            return View();
        }

        public ActionResult WebNewUser()
        {
            return View();
        }

        public ActionResult WebUsers()
        {
            return View();
        }

        public ActionResult WebUpdateUser()
        {
            return View();
        }
    }
}