﻿using LMS.Models.UserProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Controllers
{
    public class UserProfileController : Controller
    {
        // GET: UserProfile
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Update(FormCollection form)
        {
            string UserID = System.Web.HttpContext.Current.Session["RecID"].ToString();
            string password = Request.Form["password"].ToString();
            string phone = Request.Form["phone"].ToString();
            string address = Request.Form["address"].ToString();
            string submit = UserProfile.UserUpdate(address,phone,password,UserID);
            return RedirectToAction("Index");
        }

    }
}