﻿using LMS.Models.Loginuser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dbconnection.DAL;

namespace LMS.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Loginuser(string username,string password)
        {
            try
            {
                int count = 0;
                DataTable Login = Loginvalid.Logincheck(username, password);
                List<UserDetails> session = new List<UserDetails>();
                foreach (DataRow logindetails in Login.Rows)
                {
                    if (count == 0)
                    {
                        //UserDetails user = new UserDetails();
                        Session["RecID"] = logindetails["ID"].ToString();
                        Session["Name"] = logindetails["Username"].ToString();
                        Session["Phone"] = logindetails["phone"].ToString();
                        Session["Username"] = logindetails["email"].ToString();
                        Session["Password"] = logindetails["password"].ToString();
                        Session["Address"] = logindetails["address"].ToString();

                        count++;
                    }
                }
                if (Login.Rows.Count <= 0)
                {
                    return View("loginfailed");
                }
                else
                {
                    string UserID = System.Web.HttpContext.Current.Session["RecID"].ToString();
                    DataTable dt = Loginvalid.PermCheck(UserID);
                    List<Permissions> permission = new List<Permissions>();
                    foreach (DataRow dc in dt.Rows)
                    {
                        Permissions perm = new Permissions();
                        perm.PermID = dc["PermID"].ToString();
                        perm.Permission = dc["Permission"].ToString();
                        perm.PermissionDes = dc["PermissionDes"].ToString();

                        permission.Add(perm);
                        int index = permission.FindIndex(q => q.PermID == perm.PermID);
                        if (index == -1)
                        {
                            permission.Add(perm);
                            perm = new Permissions();
                        }
                    }

                    Session["Permissionlist"] = permission;
                    return RedirectToAction("Index", "Dashboard");

                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}