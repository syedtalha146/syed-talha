﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Controllers
{
    public class ClassroomController : Controller
    {
        // GET: Classroom
        public ActionResult Index()
        {
            return View();
        }

        // GET: Recording
        public ActionResult Recording()
        {
            return View();
        }

        // GET: RecordingList
        public ActionResult LecturesList()
        {
            return View();
        }

        public ActionResult Delete()
        {
            ViewBag.response = 1;
            return RedirectToAction("LecturesList");
        }

        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult Classroom()
        {
            return View();
        }
    }
}