﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LMS.Models.Loginuser;

namespace LMS.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            if (Session["RecID"] != null)
            {
                string UserID = System.Web.HttpContext.Current.Session["RecID"].ToString();
                DataTable dt = Loginvalid.usermenu(UserID);
                List<Usermenu> menus = new List<Usermenu>();
                foreach (DataRow dc in dt.Rows)
                {
                    Usermenu menu = new Usermenu();
                    menu.MenuIteam = dc["MenuIteam"].ToString();
                    menu.Icon = dc["Icon"].ToString();
                    menu.Class = dc["Class"].ToString();
                    menu.URL = dc["URL"].ToString();

                    menus.Add(menu);
                }

                ViewData["MenuDetails"] = menus;
                return View();
            }
            else
            {
                return View("SessionExpire");
            }
        }
    }
}