﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Controllers
{
    public class AssessmentController : Controller
    {
        // GET: Assessment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Test()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Result()
        {
            return View();
        }

        public ActionResult StartExam()
        {
            return View();
        }

        public ActionResult Classes()
        {
            return View();
        }

        public ActionResult Instructor()
        {
            return View();
        }

        public ActionResult ResultStatus()
        {
            return View();
        }
    }
}