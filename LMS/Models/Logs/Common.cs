﻿using dbconnection.DAL;
using LMS.Models.Loginuser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models.Logs
{
    public class Common
    {
        static DAL obj_dal = new DAL();
        public static bool checkpermission(List<Loginuser.Permissions> permission, string permissionid)
        {
            for (int i = 0; i < permission.Count; i++)
            {
                //int index = li_permisson.FindIndex(q => q.PermID == permissionid);
                if (permission[i].PermID == permissionid)
                {
                    return true;
                }
            }
            return false;

        }
    }
}