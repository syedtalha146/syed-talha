﻿using dbconnection.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LMS.Models.Tasks
{
    public class tasks
    {

        public static string AddTask(string UserID, string AssignedTo, string Task, string filepath)
        {
            try
            {
                DAL objDAL = new DAL();
                objDAL.ProcName = "PostTask";
                SPParameters spParam = new SPParameters();
                spParam.SetParam("UserID", SqlDbType.Int, UserID.ToString());
                spParam.SetParam("AssignedTo", SqlDbType.Int, AssignedTo.ToString());
                spParam.SetParam("Task", SqlDbType.NVarChar, Task.ToString());
                spParam.SetParam("Attachment", SqlDbType.NVarChar, filepath.ToString());
                string result = objDAL.AddData(spParam);
                return "1";
            }
            catch (Exception ex)
            {
                string result = ex.Message;
                return "0";
            }
        }
        public static DataTable GetMyTasks(string UserID)
        {
            try
            {
                DAL obj_dal = new DAL();
                obj_dal.ProcName = "GetMyTasks";
                SPParameters spParms = new SPParameters();
                spParms.SetParam("UserID", SqlDbType.Int, UserID.ToString());
                return obj_dal.Getdata(spParms);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetAttach(int ProfileID)
        {
            try
            {
                DAL obj_dal = new DAL();
                obj_dal.ProcName = "GetTaskAttachments";
                SPParameters spParms = new SPParameters();
                spParms.SetParam("UserID", SqlDbType.Int, ProfileID.ToString());
                return obj_dal.Getdata(spParms);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetEmail(string AssignedTo)
        {
            try
            {
                DAL objDal = new DAL();
                objDal.ProcName = "GetTaskAssignedToEmail";
                SPParameters spParam = new SPParameters();
                spParam.SetParam("UserID", SqlDbType.Int, AssignedTo.ToString());

                return objDal.AddData(spParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }

    public class MyTasks
    {
        public string Task { get; set; }
        public string TaskID { get; set; }
        public string StatusID { get; set; }
        public string AssignedTo { get; set; }
        public string AttachmentBy { get; set; }
        public string AttachmentTo { get; set; }
        public string TimeStamp { get; set; }
    }

    public class Attach
    {
        public string Attachment { get; set; }
    }

}