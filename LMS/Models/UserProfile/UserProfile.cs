﻿using dbconnection.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LMS.Models.UserProfile
{
    public class UserProfile
    {
        public static string UserUpdate(string address, string phone, string password,string UserID)
        {
            try
            {
                DAL objDal = new DAL();
                objDal.ProcName = "UpdateUserProfile";
                SPParameters spParam = new SPParameters();

                spParam.SetParam("address", SqlDbType.NVarChar, address.ToString());
                spParam.SetParam("phone", SqlDbType.NVarChar, phone.ToString());
                spParam.SetParam("password", SqlDbType.NVarChar, password.ToString());
                spParam.SetParam("UserID", SqlDbType.Int, UserID.ToString());

                return objDal.AddData(spParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}