﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mail;
namespace LMS.Models
{
    public class Gmail
    {
        [Obsolete]
        public static bool SendEmail(string pGmailEmail, string pGmailPassword, string pTo, string pSubject, string pBody, MailFormat pFormat, string pAttachmentPath)
        {
            try
            {
                 MailMessage myMail = new MailMessage();
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", "secure.emailsrvr.com");
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", "465");
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", pGmailEmail);
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", pGmailPassword);
                myMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "true");
                myMail.From = pGmailEmail;
                myMail.To = pTo;
                myMail.Subject = pSubject;
                myMail.BodyFormat = pFormat;
                myMail.Body = pBody;
                myMail.Bcc = "syedtalha_mobilelinkusa@gmail.com";

                if (pAttachmentPath.Trim() != "")
                {
                    MailAttachment MyAttachment = new MailAttachment(pAttachmentPath);
                    myMail.Attachments.Add(MyAttachment);
                    myMail.Priority = MailPriority.High;
                }
                SmtpMail.SmtpServer = "secure.emailsrvr:587";
                SmtpMail.Send(myMail);
                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}