﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dbconnection.DAL;
using System.Data;
using LMS.Models;

namespace LMS.Models.Loginuser
{
    public class Loginvalid
    {
        public static DataTable Logincheck(string username, string password)
        {
            try
            {
                DAL obj_dal = new DAL();
                obj_dal.ProcName = "GetUserByLoginCredentials";
                SPParameters spParms = new SPParameters();
                spParms.SetParam("username", SqlDbType.NVarChar, username.ToString());
                spParms.SetParam("password", SqlDbType.NVarChar, password.ToString());
                return obj_dal.Getdata(spParms);
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable();
                return dt;
                throw ex;

            }
        }

        public static DataTable usermenu(string UserID)
        {
            try
            {
                DAL obj_dal = new DAL();
                obj_dal.ProcName = "GetMenuByUser";
                SPParameters spParms = new SPParameters();
                spParms.SetParam("UserID", SqlDbType.Int, UserID.ToString());
                return obj_dal.Getdata(spParms);
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable();
                return dt;
                throw ex;

            }
        }

        public static DataTable PermCheck(string UserID)
        {
            try
            {
                DAL obj_dal = new DAL();
                obj_dal.ProcName = "GetCheckPermissionByUser";
                SPParameters spParms = new SPParameters();
                spParms.SetParam("UserID", SqlDbType.Int, UserID.ToString());
                return obj_dal.Getdata(spParms);
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable();
                return dt;
                throw ex;
            }
        }

    }

    public class UserDetails
    {
        public string Full_Name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public int IsActive { get; set; }
        public int IsDeleted { get; set; }
        public byte[] LastUpdateOn { get; set; }
    }

    public class Usermenu
    {
        public string MenuIteam { get; set; }
        public string Icon { get; set; }
        public string Class { get; set; }
        public string URL { get; set; }
    }

    public class Permissions
    {
        public string PermID { get; set; }
        public string Permission { get; set; }
        public string PermissionDes { get; set; }
    }

}
